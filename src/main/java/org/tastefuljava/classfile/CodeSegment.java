package org.tastefuljava.classfile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CodeSegment extends ByteArrayOutputStream {
    private static final Logger LOG
            = Logger.getLogger(CodeSegment.class.getName());

    protected final ConstantPool cp;
    protected short localTop;
    protected short localMax;
    protected short stackTop;
    protected short stackMax;
    protected final List<Label> labels = new ArrayList<>();
    private final List<LabelRef> fixups = new ArrayList<>();
    private final CodeSegment parent;

    protected CodeSegment(ConstantPool cp, CodeSegment parent, int locals) {
        this.cp = cp;
        this.parent = parent;
        this.localTop = this.localMax = (short)locals;
    }

    public short getLocalMax() {
        return localMax;
    }

    public short newLocal(int size) {
        short result = localTop;
        localTop += size;
        if (localTop > localMax) {
            localMax = localTop;
        }
        return result;
    }

    public short beginBlock() {
        return localTop;
    }

    public void endBlock(short b) {
        localTop = b;
    }

    public void commit() {
        for (Label label: labels) {
            label.fixupRefs(this);
        }
        if (parent != null) {
            parent.append(this);
        }
    }

    public CodeSegment newSegment() {
        return new CodeSegment(cp, this, localTop);
    }

    public short getStackMax() {
        return stackMax;
    }

    public int getLength() {
        return count;
    }

    public int getLocation() {
        return count;
    }

    public void setLocation(int newValue) {
        count = newValue;
    }

    private void append(CodeSegment other) {
        try {
            localMax = (short)Math.max(localMax, other.localMax);
            stackMax = (short)Math.max(stackMax, stackTop + other.stackMax);
            stackTop += other.stackTop;
            write(other.toByteArray());
            int offset = getLocation();
            for (LabelRef ref: other.fixups) {
                fixups.add(ref.copy(offset));
            }
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw new ClassFileException(ex.getMessage());
        }
    }

    public int getByte(int pos) {
        return buf[pos] & 0xff;
    }

    public short getShort(int pos) {
        return (short) (((buf[pos] & 0xff) << 8) | ((buf[pos + 1] & 0xff)));
    }

    public int getInt(int pos) {
        return ((buf[pos] & 0xFF) << 24)
                | ((buf[pos + 1] & 0xFF) << 16)
                | ((buf[pos + 2] & 0xff) << 8)
                | ((buf[pos + 3] & 0xff));
    }

    public void fixupByte(int pos, int value) {
        buf[pos] = (byte) value;
    }

    public void fixupShort(int pos, int value) {
        buf[pos] = (byte) (0xff & (value >>> 8));
        buf[pos + 1] = (byte) (0xff & value);
    }

    public void fixupInt(int pos, int value) {
        buf[pos] = (byte) (0xff & (value >> 24));
        buf[pos + 1] = (byte) (0xff & (value >> 16));
        buf[pos + 2] = (byte) (0xff & (value >> 8));
        buf[pos + 3] = (byte) (0xff & value);
    }

    //------------------------------------------------------------------------------
    //  constants
    //------------------------------------------------------------------------------
    public void pushInt(int value) {
        if (value >= -1 && value <= 5) {
            reserveStack(1);
            switch (value) {
                case -1:
                    opCode(ByteCode.ICONST_M1);
                    break;
                case 0:
                    opCode(ByteCode.ICONST_0);
                    break;
                case 1:
                    opCode(ByteCode.ICONST_1);
                    break;
                case 2:
                    opCode(ByteCode.ICONST_2);
                    break;
                case 3:
                    opCode(ByteCode.ICONST_3);
                    break;
                case 4:
                    opCode(ByteCode.ICONST_4);
                    break;
                case 5:
                    opCode(ByteCode.ICONST_5);
                    break;
            }
        } else if (value >= -128 && value <= 127) {
            reserveStack(1);
            opCode(ByteCode.BIPUSH);
            write(value);
        } else if (value >= -32768 && value <= 32767) {
            reserveStack(1);
            opCode(ByteCode.SIPUSH);
            writeShort(value);
        } else {
            pushConst(cp.addInteger(value));
        }
    }

    public void pushLong(long value) {
        if (value == 0) {
            reserveStack(2);
            opCode(ByteCode.LCONST_0);
        } else if (value == 1) {
            reserveStack(2);
            opCode(ByteCode.LCONST_1);
        } else if (value >= -128 && value <= 127) {
            pushInt((int) value);
            intToLong();
        } else {
            pushConst2(cp.addLong(value));
        }
    }

    public void pushFloat(float value) {
        if (value == 0.0) {
            reserveStack(1);
            opCode(ByteCode.FCONST_0);
        } else if (value == 1.0) {
            reserveStack(1);
            opCode(ByteCode.FCONST_1);
        } else if (value == 2.0) {
            reserveStack(1);
            opCode(ByteCode.FCONST_2);
        } else {
            pushConst(cp.addFloat(value));
        }
    }

    public void pushDouble(double value) {
        if (value == 0.0) {
            reserveStack(2);
            opCode(ByteCode.DCONST_0);
        } else if (value == 1.0) {
            reserveStack(2);
            opCode(ByteCode.DCONST_1);
        } else {
            pushConst2(cp.addDouble(value));
        }
    }

    public void pushNull() {
        reserveStack(1);
        opCode(ByteCode.ACONST_NULL);
    }

    public void pushString(String s) {
        pushConst(cp.addString(s));
    }

    //------------------------------------------------------------------------------
    //  local variables
    //------------------------------------------------------------------------------
    public void loadInt(int index) {
        reserveStack(1);
        switch (index) {
            case 0:
                opCode(ByteCode.ILOAD_0);
                break;
            case 1:
                opCode(ByteCode.ILOAD_1);
                break;
            case 2:
                opCode(ByteCode.ILOAD_2);
                break;
            case 3:
                opCode(ByteCode.ILOAD_3);
                break;
            default:
                localOp(ByteCode.ILOAD, index);
        }
    }

    public void loadFloat(int index) {
        reserveStack(1);
        switch (index) {
            case 0:
                opCode(ByteCode.FLOAD_0);
                break;
            case 1:
                opCode(ByteCode.FLOAD_1);
                break;
            case 2:
                opCode(ByteCode.FLOAD_2);
                break;
            case 3:
                opCode(ByteCode.FLOAD_3);
                break;
            default:
                localOp(ByteCode.FLOAD, index);
        }
    }

    public void loadLong(int index) {
        reserveStack(2);
        switch (index) {
            case 0:
                opCode(ByteCode.LLOAD_0);
                break;
            case 1:
                opCode(ByteCode.LLOAD_1);
                break;
            case 2:
                opCode(ByteCode.LLOAD_2);
                break;
            case 3:
                opCode(ByteCode.LLOAD_3);
                break;
            default:
                localOp(ByteCode.LLOAD, index);
        }
    }

    public void loadDouble(int index) {
        reserveStack(2);
        switch (index) {
            case 0:
                opCode(ByteCode.DLOAD_0);
                break;
            case 1:
                opCode(ByteCode.DLOAD_1);
                break;
            case 2:
                opCode(ByteCode.DLOAD_2);
                break;
            case 3:
                opCode(ByteCode.DLOAD_3);
                break;
            default:
                localOp(ByteCode.DLOAD, index);
        }
    }

    public void loadRef(int index) {
        reserveStack(1);
        switch (index) {
            case 0:
                opCode(ByteCode.ALOAD_0);
                break;
            case 1:
                opCode(ByteCode.ALOAD_1);
                break;
            case 2:
                opCode(ByteCode.ALOAD_2);
                break;
            case 3:
                opCode(ByteCode.ALOAD_3);
                break;
            default:
                localOp(ByteCode.ALOAD, index);
        }
    }

    public void storeInt(int index) {
        releaseStack(1);
        switch (index) {
            case 0:
                opCode(ByteCode.ISTORE_0);
                break;
            case 1:
                opCode(ByteCode.ISTORE_1);
                break;
            case 2:
                opCode(ByteCode.ISTORE_2);
                break;
            case 3:
                opCode(ByteCode.ISTORE_3);
                break;
            default:
                localOp(ByteCode.ISTORE, index);
        }
    }

    public void storeFloat(int index) {
        releaseStack(1);
        switch (index) {
            case 0:
                opCode(ByteCode.FSTORE_0);
                break;
            case 1:
                opCode(ByteCode.FSTORE_1);
                break;
            case 2:
                opCode(ByteCode.FSTORE_2);
                break;
            case 3:
                opCode(ByteCode.FSTORE_3);
                break;
            default:
                localOp(ByteCode.FSTORE, index);
        }
    }

    public void storeLong(int index) {
        releaseStack(2);
        switch (index) {
            case 0:
                opCode(ByteCode.LSTORE_0);
                break;
            case 1:
                opCode(ByteCode.LSTORE_1);
                break;
            case 2:
                opCode(ByteCode.LSTORE_2);
                break;
            case 3:
                opCode(ByteCode.LSTORE_3);
                break;
            default:
                localOp(ByteCode.LSTORE, index);
        }
    }

    public void storeDouble(int index) {
        releaseStack(2);
        switch (index) {
            case 0:
                opCode(ByteCode.DSTORE_0);
                break;
            case 1:
                opCode(ByteCode.DSTORE_1);
                break;
            case 2:
                opCode(ByteCode.DSTORE_2);
                break;
            case 3:
                opCode(ByteCode.DSTORE_3);
                break;
            default:
                localOp(ByteCode.DSTORE, index);
        }
    }

    public void storeRef(int index) {
        releaseStack(1);
        switch (index) {
            case 0:
                opCode(ByteCode.ASTORE_0);
                break;
            case 1:
                opCode(ByteCode.ASTORE_1);
                break;
            case 2:
                opCode(ByteCode.ASTORE_2);
                break;
            case 3:
                opCode(ByteCode.ASTORE_3);
                break;
            default:
                localOp(ByteCode.ASTORE, index);
        }
    }

    public void incInt(int index, int increment) {
        if (index <= 255 && increment >= -128 && increment <= 127) {
            opCode(ByteCode.IINC);
            write(index);
            write(increment);
        } else {
            opCode(ByteCode.WIDE);
            opCode(ByteCode.IINC);
            writeShort(index);
            writeShort(increment);
        }
    }

    //------------------------------------------------------------------------------
    //  fields
    //------------------------------------------------------------------------------
    public void getField(short refIndex) {
        releaseStack(1);
        String type = cp.getRefType(refIndex);
        if (type.equals("J") || type.equals("D")) {
            reserveStack(2);
        } else {
            reserveStack(1);
        }
        opCode(ByteCode.GETFIELD);
        writeShort(refIndex);
    }

    public void getField(short classIndex, short natIndex) {
        getField(cp.addFieldref(classIndex, natIndex));
    }

    public void getField(short classIndex, short nameIndex, short typeIndex) {
        getField(cp.addFieldref(classIndex, nameIndex, typeIndex));
    }

    public void getField(short classIndex, String name, String type) {
        getField(cp.addFieldref(classIndex, name, type));
    }

    public void getField(String className, String name, String type) {
        getField(cp.addFieldref(className, name, type));
    }

    public void putField(short refIndex) {
        releaseStack(1);
        String type = cp.getRefType(refIndex);
        if (type.equals("J") || type.equals("D")) {
            releaseStack(2);
        } else {
            releaseStack(1);
        }
        opCode(ByteCode.PUTFIELD);
        writeShort(refIndex);
    }

    public void putField(short classIndex, short natIndex) {
        putField(cp.addFieldref(classIndex, natIndex));
    }

    public void putField(short classIndex, short nameIndex, short typeIndex) {
        putField(cp.addFieldref(classIndex, nameIndex, typeIndex));
    }

    public void putField(short classIndex, String name, String type) {
        putField(cp.addFieldref(classIndex, name, type));
    }

    public void putField(String className, String name, String type) {
        putField(cp.addFieldref(className, name, type));
    }

    public void getStatic(short refIndex) {
        String type = cp.getRefType(refIndex);
        if (type.equals("J") || type.equals("D")) {
            reserveStack(2);
        } else {
            reserveStack(1);
        }
        opCode(ByteCode.GETSTATIC);
        writeShort(refIndex);
    }

    public void getStatic(short classIndex, short natIndex) {
        getStatic(cp.addFieldref(classIndex, natIndex));
    }

    public void getStatic(short classIndex, short nameIndex, short typeIndex) {
        getStatic(cp.addFieldref(classIndex, nameIndex, typeIndex));
    }

    public void getStatic(short classIndex, String name, String type) {
        getStatic(cp.addFieldref(classIndex, name, type));
    }

    public void getStatic(String className, String name, String type) {
        getStatic(cp.addFieldref(className, name, type));
    }

    public void putStatic(short refIndex) {
        String type = cp.getRefType(refIndex);
        if (type.equals("J") || type.equals("D")) {
            releaseStack(2);
        } else {
            releaseStack(1);
        }
        opCode(ByteCode.PUTSTATIC);
        writeShort(refIndex);
    }

    public void putStatic(short classIndex, short natIndex) {
        putStatic(cp.addFieldref(classIndex, natIndex));
    }

    public void putStatic(short classIndex, short nameIndex, short typeIndex) {
        putStatic(cp.addFieldref(classIndex, nameIndex, typeIndex));
    }

    public void putStatic(short classIndex, String name, String type) {
        putStatic(cp.addFieldref(classIndex, name, type));
    }

    public void putStatic(String className, String name, String type) {
        putStatic(cp.addFieldref(className, name, type));
    }

    //------------------------------------------------------------------------------
    //  methods
    //------------------------------------------------------------------------------
    public void invokeInterface(short refIndex) {
        releaseStack(1); /* for the 'this' reference */
        int asize = invokeStack(cp.getRefType(refIndex));
        opCode(ByteCode.INVOKEINTERFACE);
        writeShort(refIndex);
        write(asize + 1);
        write(0);
    }

    public void invokeInterface(short classIndex, short natIndex) {
        invokeInterface(cp.addInterfaceMethodref(classIndex, natIndex));
    }

    public void invokeInterface(short classIndex, short nameIndex, short typeIndex) {
        invokeInterface(cp.addInterfaceMethodref(classIndex, nameIndex, typeIndex));
    }

    public void invokeInterface(short classIndex, String name, String type) {
        invokeInterface(cp.addInterfaceMethodref(classIndex, name, type));
    }

    public void invokeInterface(String className, String name, String type) {
        invokeInterface(cp.addInterfaceMethodref(className, name, type));
    }

    public void invokeSpecial(short refIndex) {
        releaseStack(1); /* for the 'this' reference */
        invokeStack(cp.getRefType(refIndex));
        opCode(ByteCode.INVOKESPECIAL);
        writeShort(refIndex);
    }

    public void invokeSpecial(short classIndex, short natIndex) {
        invokeSpecial(cp.addMethodref(classIndex, natIndex));
    }

    public void invokeSpecial(short classIndex, short nameIndex, short typeIndex) {
        invokeSpecial(cp.addMethodref(classIndex, nameIndex, typeIndex));
    }

    public void invokeSpecial(short classIndex, String name, String type) {
        invokeSpecial(cp.addMethodref(classIndex, name, type));
    }

    public void invokeSpecial(String className, String name, String type) {
        invokeSpecial(cp.addMethodref(className, name, type));
    }

    public void invokeVirtual(short refIndex) {
        releaseStack(1); /* for the 'this' reference */
        invokeStack(cp.getRefType(refIndex));
        opCode(ByteCode.INVOKEVIRTUAL);
        writeShort(refIndex);
    }

    public void invokeVirtual(short classIndex, short natIndex) {
        invokeVirtual(cp.addMethodref(classIndex, natIndex));
    }

    public void invokeVirtual(short classIndex, short nameIndex, short typeIndex) {
        invokeVirtual(cp.addMethodref(classIndex, nameIndex, typeIndex));
    }

    public void invokeVirtual(short classIndex, String name, String type) {
        invokeVirtual(cp.addMethodref(classIndex, name, type));
    }

    public void invokeVirtual(String className, String name, String type) {
        invokeVirtual(cp.addMethodref(className, name, type));
    }

    public void invokeStatic(short refIndex) {
        invokeStack(cp.getRefType(refIndex));
        opCode(ByteCode.INVOKESTATIC);
        writeShort(refIndex);
    }

    public void invokeStatic(short classIndex, short natIndex) {
        invokeStatic(cp.addMethodref(classIndex, natIndex));
    }

    public void invokeStatic(short classIndex, short nameIndex, short typeIndex) {
        invokeStatic(cp.addMethodref(classIndex, nameIndex, typeIndex));
    }

    public void invokeStatic(short classIndex, String name, String type) {
        invokeStatic(cp.addMethodref(classIndex, name, type));
    }

    public void invokeStatic(String className, String name, String type) {
        invokeStatic(cp.addMethodref(className, name, type));
    }

    public void returnVoid() {
        opCode(ByteCode.RETURN);
    }

    public void returnInt() {
        releaseStack(1);
        opCode(ByteCode.IRETURN);
    }

    public void returnFloat() {
        releaseStack(1);
        opCode(ByteCode.FRETURN);
    }

    public void returnLong() {
        releaseStack(2);
        opCode(ByteCode.LRETURN);
    }

    public void returnDouble() {
        releaseStack(2);
        opCode(ByteCode.DRETURN);
    }

    public void returnRef() {
        releaseStack(1);
        opCode(ByteCode.ARETURN);
    }

    //------------------------------------------------------------------------------
    //  array operations
    //------------------------------------------------------------------------------
    public void makeRefArray(short typeIndex) {
        opCode(ByteCode.ANEWARRAY);
        writeShort(typeIndex);
    }

    public void makeRefArray(String type) {
        makeRefArray(cp.addClass(type));
    }

    public void arrayLength() {
        opCode(ByteCode.ARRAYLENGTH);
    }

    public void loadArrayInt() {
        releaseStack(1);
        opCode(ByteCode.IALOAD);
    }

    public void loadArrayLong() {
        opCode(ByteCode.LALOAD);
    }

    public void loadArrayFloat() {
        releaseStack(1);
        opCode(ByteCode.FALOAD);
    }

    public void loadArrayDouble() {
        opCode(ByteCode.DALOAD);
    }

    public void loadArrayRef() {
        releaseStack(1);
        opCode(ByteCode.AALOAD);
    }

    public void storeArrayInt() {
        releaseStack(3);
        opCode(ByteCode.IASTORE);
    }

    public void storeArrayLong() {
        releaseStack(4);
        opCode(ByteCode.LASTORE);
    }

    public void storeArrayFloat() {
        releaseStack(3);
        opCode(ByteCode.FASTORE);
    }

    public void storeArrayDouble() {
        releaseStack(4);
        opCode(ByteCode.DASTORE);
    }

    public void storeArrayRef() {
        releaseStack(3);
        opCode(ByteCode.AASTORE);
    }

    //------------------------------------------------------------------------------
    //  conversions
    //------------------------------------------------------------------------------
    public void intToByte() {
        opCode(ByteCode.I2B);
    }

    public void intToChar() {
        opCode(ByteCode.I2C);
    }

    public void intToShort() {
        opCode(ByteCode.I2F);
    }

    public void intToFloat() {
        opCode(ByteCode.I2F);
    }

    public void intToLong() {
        reserveStack(1);
        opCode(ByteCode.I2L);
    }

    public void intToDouble() {
        reserveStack(1);
        opCode(ByteCode.I2D);
    }

    public void floatToDouble() {
        reserveStack(1);
        opCode(ByteCode.F2D);
    }

    public void floatToLong() {
        reserveStack(1);
        opCode(ByteCode.F2L);
    }

    public void floatToInt() {
        opCode(ByteCode.F2I);
    }

    public void longToDouble() {
        opCode(ByteCode.L2D);
    }

    public void longToFloat() {
        releaseStack(1);
        opCode(ByteCode.L2F);
    }

    public void longToInt() {
        releaseStack(1);
        opCode(ByteCode.L2I);
    }

    public void doubleToLong() {
        opCode(ByteCode.D2L);
    }

    public void doubleToFloat() {
        releaseStack(1);
        opCode(ByteCode.D2F);
    }

    public void doubleToInt() {
        releaseStack(1);
        opCode(ByteCode.D2I);
    }

    //------------------------------------------------------------------------------
    //  stack operations
    //------------------------------------------------------------------------------
    public void pop() {
        releaseStack(1);
        opCode(ByteCode.POP);
    }

    public void pop2() {
        releaseStack(2);
        opCode(ByteCode.POP2);
    }

    public void swap() {
        opCode(ByteCode.SWAP);
    }

    public void dup() {
        reserveStack(1);
        opCode(ByteCode.DUP);
    }

    public void dupX1() {
        reserveStack(1);
        opCode(ByteCode.DUP_X1);
    }

    public void dupX2() {
        reserveStack(1);
        opCode(ByteCode.DUP_X2);
    }

    public void dup2() {
        reserveStack(2);
        opCode(ByteCode.DUP2);
    }

    public void dup2X1() {
        reserveStack(2);
        opCode(ByteCode.DUP2_X1);
    }

    public void dup2X2() {
        reserveStack(2);
        opCode(ByteCode.DUP2_X2);
    }

    //------------------------------------------------------------------------------
    //  objects
    //------------------------------------------------------------------------------
    public void checkCast(int classIndex) {
        opCode(ByteCode.CHECKCAST);
        writeShort(classIndex);
    }

    public void checkCast(String className) {
        checkCast(cp.addClass(className));
    }

    public void newObject(int classIndex) {
        reserveStack(1);
        opCode(ByteCode.NEW);
        writeShort(classIndex);
    }

    public void newObject(String className) {
        newObject(cp.addClass(className));
    }

    //------------------------------------------------------------------------------
    //  exceptions
    //------------------------------------------------------------------------------
    public void throwRef() {
        releaseStack(1);
        opCode(ByteCode.ATHROW);
    }

    //------------------------------------------------------------------------------
    //  arithmetic operations
    //------------------------------------------------------------------------------
    public void addInt() {
        releaseStack(1);
        opCode(ByteCode.IADD);
    }

    public void andInt() {
        releaseStack(1);
        opCode(ByteCode.IAND);
    }

    public void divInt() {
        releaseStack(1);
        opCode(ByteCode.IDIV);
    }

    public void mulInt() {
        releaseStack(1);
        opCode(ByteCode.IMUL);
    }

    public void negInt() {
        opCode(ByteCode.INEG);
    }

    public void orInt() {
        releaseStack(1);
        opCode(ByteCode.IOR);
    }

    public void remInt() {
        releaseStack(1);
        opCode(ByteCode.IREM);
    }

    public void shlInt() {
        releaseStack(1);
        opCode(ByteCode.ISHL);
    }

    public void shrInt() {
        releaseStack(1);
        opCode(ByteCode.ISHR);
    }

    public void subInt() {
        releaseStack(1);
        opCode(ByteCode.ISUB);
    }

    public void ushrInt() {
        releaseStack(1);
        opCode(ByteCode.IUSHR);
    }

    public void xorInt() {
        releaseStack(1);
        opCode(ByteCode.IXOR);
    }

    public void addLong() {
        releaseStack(2);
        opCode(ByteCode.LADD);
    }

    public void andLong() {
        releaseStack(2);
        opCode(ByteCode.LAND);
    }

    public void divLong() {
        releaseStack(2);
        opCode(ByteCode.LDIV);
    }

    public void mulLong() {
        releaseStack(2);
        opCode(ByteCode.LMUL);
    }

    public void negLong() {
        opCode(ByteCode.LNEG);
    }

    public void orLong() {
        releaseStack(2);
        opCode(ByteCode.LOR);
    }

    public void remLong() {
        releaseStack(2);
        opCode(ByteCode.LREM);
    }

    public void shlLong() {
        releaseStack(1);
        opCode(ByteCode.LSHL);
    }

    public void shrLong() {
        releaseStack(1);
        opCode(ByteCode.LSHR);
    }

    public void subLong() {
        releaseStack(2);
        opCode(ByteCode.LSUB);
    }

    public void ushrLong() {
        releaseStack(1);
        opCode(ByteCode.LUSHR);
    }

    public void xorLong() {
        releaseStack(2);
        opCode(ByteCode.LXOR);
    }

    public void addFloat() {
        releaseStack(1);
        opCode(ByteCode.FADD);
    }

    public void cmpgFloat() {
        releaseStack(1);
        opCode(ByteCode.FCMPG);
    }

    public void cmplFloat() {
        releaseStack(1);
        opCode(ByteCode.FCMPL);
    }

    public void divFloat() {
        releaseStack(1);
        opCode(ByteCode.FDIV);
    }

    public void mulFloat() {
        releaseStack(1);
        opCode(ByteCode.FMUL);
    }

    public void negFloat() {
        opCode(ByteCode.FNEG);
    }

    public void remFloat() {
        releaseStack(1);
        opCode(ByteCode.FREM);
    }

    public void subFloat() {
        releaseStack(1);
        opCode(ByteCode.ISUB);
    }

    public void addDouble() {
        releaseStack(2);
        opCode(ByteCode.DADD);
    }

    public void cmpgDouble() {
        releaseStack(2);
        opCode(ByteCode.DCMPG);
    }

    public void cmplDouble() {
        releaseStack(2);
        opCode(ByteCode.DCMPL);
    }

    public void divDouble() {
        releaseStack(2);
        opCode(ByteCode.DDIV);
    }

    public void mulDouble() {
        releaseStack(2);
        opCode(ByteCode.DMUL);
    }

    public void negDouble() {
        opCode(ByteCode.DNEG);
    }

    public void remDouble() {
        releaseStack(2);
        opCode(ByteCode.DREM);
    }

    public void subDouble() {
        releaseStack(2);
        opCode(ByteCode.DSUB);
    }

    //------------------------------------------------------------------------------
    //  labels and jumps
    //------------------------------------------------------------------------------
    public void define(Label label) {
        label.define(getLocation());
        labels.add(label);
    }

    public void jump(Label label) {
        jump(ByteCode.GOTO, label);
    }

    public void jump(ByteCode opcode, Label label) {
        /* adjust stack */
        switch (opcode) {
            case IF_ICMPEQ:
            case IF_ICMPNE:
            case IF_ICMPLT:
            case IF_ICMPGE:
            case IF_ICMPGT:
            case IF_ICMPLE:
            case IF_ACMPEQ:
            case IF_ACMPNE:
                releaseStack(2);
                break;
            case IFNULL:
            case IFNONNULL:
            case IFEQ:
            case IFNE:
            case IFLT:
            case IFGE:
            case IFGT:
            case IFLE:
                releaseStack(1);
                break;
            case GOTO:
                break;
            default:
                throw new InvalidInstructionException(opcode.getCode());
        }
        LabelRef ref = new JumpRef(getLocation());
        addRef(label, ref);
        opCode(opcode);
        writeShort(0);
    }

    public void tableSwitch(int min, int max, Label def, Label[] table) {
        if (max - min + 1 != table.length) {
            throw new TableSwitchException();
        }
        int opLocation = getLocation();
        opCode(ByteCode.TABLESWITCH);
        while ((getLocation() % 4) != 0) {
            write(0);
        }
        addRef(def, new TableSwitchRef(opLocation, getLocation()));
        writeInt(0);
        writeInt(min);
        writeInt(max);
        for (int i = 0; i < table.length; ++i) {
            addRef(table[i], new TableSwitchRef(opLocation, getLocation()));
            writeInt(0);
        }
    }

    //------------------------------------------------------------------------------
    //  private stuff
    //------------------------------------------------------------------------------
    void writeShort(int value) {
        write((byte) (value >>> 8));
        write((byte) value);
    }

    void writeInt(int value) {
        write((byte) (value >>> 24));
        write((byte) (value >>> 16));
        write((byte) (value >>> 8));
        write((byte) value);
    }

    void reserveStack(int count) {
        stackTop += count;
        if (stackTop > stackMax) {
            stackMax = stackTop;
        }
    }

    void releaseStack(int count) {
        stackTop -= count;
    }

    protected void pushConst(int index) {
        reserveStack(1);
        if (index <= 255) {
            opCode(ByteCode.LDC);
            write(index);
        } else {
            opCode(ByteCode.LDC_W);
            writeShort(index);
        }
    }

    protected void pushConst2(int index) {
        reserveStack(2);
        opCode(ByteCode.LDC2_W);
        writeShort(index);
    }

    protected void opCode(ByteCode bc) {
        write(bc.getCode());
    }

    protected void localOp(ByteCode opcode, int index) {
        if (index <= 255) {
            opCode(opcode);
            write((byte) index);
        } else {
            opCode(ByteCode.WIDE);
            opCode(opcode);
            writeShort(index);
        }
    }

    /**
     * updates the stack pointer for a method invocation
     * @param type descriptor of the function
     * @return number of slots required by the arguments
     */
    protected int invokeStack(String type) {
        try {
            Reader reader = new StringReader(type);
            int argsSize = 0;
            int c = reader.read();
            if (c != '(') {
                throw new RuntimeException("Invalid method descriptor: " + type);
            }
            c = reader.read();
            while (c >= 0 && c != ')') {
                switch (c) {
                    case 'D':
                    case 'J':
                        argsSize += 2;
                        break;
                    default:
                        ++argsSize;
                }
                while (c == '[') {
                    c = reader.read();
                }
                if (c == 'L') {
                    do {
                        c = reader.read();
                    } while (c >= 0 && c != ';');
                }
                if (c < 0) {
                    break;
                }
                c = reader.read();
            }
            if (c != ')') {
                throw new RuntimeException("Invalid method descriptor: " + type);
            }
            releaseStack(argsSize);
            c = reader.read();
            if (c < 0) {
                throw new RuntimeException("Invalid method descriptor: " + type);
            }
            switch (c) {
                case 'V':
                    break;
                case 'D':
                case 'J':
                    reserveStack(2);
                    break;
                default:
                    reserveStack(1);
            }
            return argsSize;
        } catch (IOException e) {
            /* should never happend */
            throw new Error(e.toString());
        }
    }

    void addRef(Label label, LabelRef ref) {
        fixups.add(ref);
        label.addRef(ref);
    }

    void removeRef(LabelRef ref) {
        fixups.remove(ref);
    }
}
